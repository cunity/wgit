"""Keep track of your git repos on the command line"""
import argparse
import platform
import subprocess
from typing import Tuple

import yaml
import os
from pathlib import Path

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("--add", action="store_true", default=False,
                    help="add the current working directory as a repo")
parser.add_argument("--remove", type=str,
                    help="remove the given repo from wgit")


INITIAL_DIR = Path.cwd()
HOME = os.path.expanduser("~")
CFG_FILE = Path(HOME) / ".wgit" / "repos.yaml"


def load_repos() -> []:
    if CFG_FILE.is_file():
        with open(CFG_FILE, "r") as ydata:
            return yaml.safe_load(ydata)
    return []


def save_repos(data: list) -> str:
    folder = CFG_FILE.parent
    if not folder.exists():
        folder.mkdir(parents=True)
    with open(CFG_FILE, "w") as ydata:
        yaml.safe_dump(data, ydata)
    return str(CFG_FILE)


def get_repo_info(repo: Path) -> Tuple[str, str]:
    branch = "unknown"
    commit = ""
    try:
        branch = subprocess.check_output(["git", "rev-parse", "--abbrev-ref", "HEAD"], cwd=str(repo), encoding="utf-8", stderr=subprocess.DEVNULL)
        commit = subprocess.check_output(["git", "log", "-n1", "--oneline"], cwd=str(repo), encoding="utf-8", stderr=subprocess.DEVNULL)
    except OSError:
        pass
    except subprocess.CalledProcessError:
        pass
    return branch.strip(), commit.strip()


def add_repo():
    repos = set(load_repos())
    repos.remove(str(INITIAL_DIR))
    save_repos(list(repos))


def del_repo(repo: str):
    repos = set(load_repos())
    if repo in repos:
        del repos[repo]
    save_repos(list(repos))


def list_repos():
    repos = load_repos()
    for repo in repos:
        branch, commit = get_repo_info(repo)
        print(f"{str(repo):48} {branch} : {commit}")


def run(args=None):
    os.chdir(str(HOME))
    opts = parser.parse_args(args)

    if opts.add:
        add_repo()
    elif opts.remove:
        del_repo(opts.remove)
    else:
        list_repos()


if __name__ == "__main__":
    run()
